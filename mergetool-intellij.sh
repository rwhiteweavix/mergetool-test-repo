git config merge.tool intellij
git config mergetool.intellij.cmd '/Applications/Android\\ Studio.app/Contents/MacOS/studio merge $(cd $(dirname \"$LOCAL\") && pwd)/$(basename \"$LOCAL\") $(cd $(dirname \"$REMOTE\") && pwd)/$(basename \"$REMOTE\") $(cd $(dirname \"$BASE\") && pwd)/$(basename \"$BASE\") $(cd $(dirname \"$MERGED\") && pwd)/$(basename \"$MERGED\")'
git config mergetool.intellij.trustExitCode true
